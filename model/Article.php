<?php

class Article
{
    private function getConnection()
    {
        $pdoConnection = new PDO('pgsql:host=127.0.0.1;dbname=postgres', 'postgres', 'famulie');

        return $pdoConnection;
    }

    public function create($name, $description , $created_at)
    {
        $pdo = new PDO("pgsql:host=127.0.0.1;dbname=postgres", 'postgres', 'famulie');
        $stmt = $pdo->prepare('INSERT INTO article (name, description, created_at) VALUES (:name,:description,:created_at)');
        $stmt->bindValue(':name', $name);
        $stmt->bindValue(':description', $description );
        $stmt->bindValue(':created_at', $created_at);
        $stmt->execute();
        $stmt->errorInfo();
    }
    public function findAll()
    {
        $pdo = new PDO("pgsql:host=127.0.0.1;dbname=postgres", 'postgres', 'famulie');
        $stmt = $pdo->prepare('SELECT * FROM article');
        $stmt->execute();
        $articles = $stmt->fetchAll();
        return $articles;
    }
    public function update($id , $name , $description , $created_at )
    {
        $pdo = new PDO("pgsql:host=127.0.0.1;dbname=postgres", 'postgres', 'famulie');
        $stmt = $pdo->prepare('UPDATE article SET name = :name, description = :description, created_at = :created_at WHERE id = :id');
        $stmt->bindValue(':id', $id);
        $stmt->bindValue(':name',$name);
        $stmt->bindValue(':description',$description);
        $stmt->bindValue(':created_at', $created_at );
        $stmt->execute();
    }
    public function findByld($id)
    {
        $pdo = new PDO("pgsql:host=127.0.0.1;dbname=postgres", 'postgres', 'famulie');
        $stmt = $pdo->prepare('SELECT * FROM article WHERE id = :id');
        $stmt->bindValue(':id',$id);
        $stmt->execute();
        $article = $stmt->fetch();
        return $article;
    }
    public function deleteByld($id)
    {
        $pdo = new PDO("pgsql:host=127.0.0.1;dbname=postgres", 'postgres', 'famulie');
        $stmt = $pdo->prepare('DELETE FROM article WHERE id = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    }
}
