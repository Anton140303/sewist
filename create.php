<?php
require_once "model/Article.php";
if (isset($_POST['name']) && isset($_POST['description']) && isset($_POST['created_at'])) {
    $article = new Article();
    $article->create($_POST['name'], $_POST['description'], $_POST['created_at']);
    header('Location: index.php');
}
$article['name'] = "";
$article['description'] = "";
$article['created_at'] = date('Y-m-d H:i:s', time());

require_once "view/updateTemplate.php";
